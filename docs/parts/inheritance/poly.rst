.. _polymorphic_inheritance:

Polymorphic Inheritance
=======================

>>> from frankfurt.models import Model
>>> from frankfurt import fields
>>>
>>> class Place(Model):
...     name = fields.CharField(max_length=400, not_null=True)
>>>
>>> class Restaurant(Place):
...     pass
>>>
