.. _simple_inheritance:

Inheritance
===========

The functionality of a model can be extended via inheritance from a
base class that has been marked to be abstract. For example:

>>> from frankfurt.models import Model
>>> from frankfurt import fields
>>>
>>> class BaseModel(Model):
...     pk = fields.UUIDField(primary_key=True)
...
...     class Meta:
...         abstract = True
>>>
>>> class Book(BaseModel):
...     title = fields.CharField(max_length=1000, not_null=True)
>>>

When a model B extends model A via inheritance several things happen:

1. All the fields of model A are copied into model B, this includes
   any model marked as primary key.
