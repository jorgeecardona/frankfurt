.. _fields:

======
Fields
======

`Frankfurt` supports the following fields.

CharField
=========

This maps to a ``VARCHAR`` column in postgres.

UUIDField
=========

This maps to a ``UUID`` column in postgres.

IntegerField
============

This field maps to an ``INTEGER`` column in PostgreSQL. It can validate against
non-negative values::

>>> from frankfurt import fields
>>>
>>> age = fields.IntegerField(non_negative=True, not_null=True)

>>> age.assert_type(-1)
Traceback (most recent call last):
...
TypeError: Value is negative.

>>> age.assert_type(None)
Traceback (most recent call last):
...
TypeError: Value is None.

>>> age.assert_type(28)
True

DateTimeField
=============

This field maps to a ``TIMESTAMPTZ`` column in PostgreSQL. It validates for
time zone aware datetime instances::

>>> from frankfurt import fields
>>> from datetime import datetime, timezone
>>>
>>> created = fields.DateTimeField(not_null=True)

>>> created.assert_type(datetime.now())
Traceback (most recent call last):
...
TypeError: Value is a naive datetime instance.

>>> created.assert_type(datetime.now(timezone.utc))
True
