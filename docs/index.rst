.. Frankfurt documentation master file, created by
   sphinx-quickstart on Sat Apr  4 01:10:28 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Frankfurt's documentation!
=====================================

.. include:: ../README.rst
  :start-after: inclusion-marker-do-not-remove


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   user/intro
   user/install
   user/quickstart
   parts/fields
   parts/inheritance/simple
   parts/inheritance/poly
   reference

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
