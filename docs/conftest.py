from mock import patch, MagicMock, AsyncMock


# Mock a connection pool.
p = patch('frankfurt.database.asyncpg.create_pool', new=AsyncMock())


def pytest_runtest_setup(item):
    m = p.start()
    m.return_value.acquire = AsyncMock(return_value = MagicMock())
    conn = m.return_value.acquire.return_value
    conn.__aenter__.return_value = conn
    tr = AsyncMock()
    tr.start = AsyncMock()
    conn.transaction = MagicMock(return_value=tr)
    conn.execute = AsyncMock()
    conn.fetchrow = AsyncMock(return_value={})
    conn.fetch = AsyncMock(return_value=[])


def pytest_runtest_teardown(item, nextitem):
    p.stop()
