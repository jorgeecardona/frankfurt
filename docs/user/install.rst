.. _install:

Installation
============

The simplest way to install `frankfurt` is to use `pip`:

.. code:: bash

    pip install frankfurt
