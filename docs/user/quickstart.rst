.. _quickstart:

Quickstart
==========

This page gives you a basic instroduction on how to use `frankfurt`.


Models
------

A simple model can be defined used the class :class:`frankfurt.models.Model` as follows:

>>> from frankfurt.models import Model, InsertMixin
>>> from frankfurt import fields
>>>
>>> class Book(Model, InsertMixin):
...    title = fields.CharField(max_length=400, not_null=True)
>>>
>>> book = Book()

So far we just have an instance of a book, without a title.

>>> book['title']
Traceback (most recent call last):
...
KeyError: 'Field title has no value.'
>>> book['title'] = 'Python'

Instances of :class:`frankfurt.Model` come with the method `:meth:`Model.save` that receives optionally an asyncpg connection object to actually execute the insert query into the database. For example:

>>> import asyncio
>>> import asyncpg
>>> from frankfurt import Database
>>>
>>> async def _():
...
...     db = await Database('postgres://user:password@server/db', models=[Book])
...
...     # Create a session.
...     conn = await db.acquire()
...
...     # Same as conn.insert(book)
...     return await book.insert(conn)
...
>>> asyncio.run(_())
<Book />
>>> book['title']
'Python'
