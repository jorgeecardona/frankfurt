

class FrankfurtException(Exception):
    pass


class EmptySelection(Exception):
    pass


class MultipleRowsSelected(Exception):
    pass
